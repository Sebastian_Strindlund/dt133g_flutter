import 'package:flutter/material.dart';

Widget buildBubblesortButton(BuildContext context, String type, int size) {
  return SizedBox(
    width: 150,
    child: RaisedButton(
      elevation: 15,
      color: Colors.cyan,
      textColor: Colors.white,
      child: Text(
        '$type BUBBLESORT(${(size / 1000).toStringAsFixed(0)}K)',
        textAlign: TextAlign.center,
        style: TextStyle(fontSize: 13),
        
      ),
      onPressed: () {
        List<int> testArray = List<int>.generate(size, (int index) => index);

        if (type == "WORST CASE") {
          testArray = testArray.reversed.toList();
        }

        List<int> values;
        int start, stop, elapsed;
        values = testArray.toList();

        start = DateTime.now().millisecondsSinceEpoch;
        for (int i = 0; i < values.length - 1; i++) {
          for (int j = 0; j < values.length - 1; j++) {
            if (values[j] > values[j + 1]) {
              int temp = values[j];
              values[j] = values[j + 1];
              values[j + 1] = temp;
            }
          }
        }
        stop = new DateTime.now().millisecondsSinceEpoch;
        elapsed = stop - start;
        print(
            "** Flutter Version - Bubblesort with ${(testArray.length / 1000).toStringAsFixed(0)}K ($type): time: $elapsed ms **");
      },
    ),
  );
}
