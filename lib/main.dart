import 'package:flutter/material.dart';

import 'bubble.dart';
import 'fibonacci.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'DT133G_Flutter',
      theme: ThemeData(
        primarySwatch: Colors.indigo,
      ),
      routes: {
        '/': (BuildContext context) => LandingPage(),
      },
    );
  }
}

class LandingPage extends StatefulWidget {
  // const LandingPage({Key key, this.model}): super(key: key);

  @override
  _LandingPageState createState() => _LandingPageState();
}

class _LandingPageState extends State<LandingPage> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("DT133G Flutter Version"),
      ),
      body:  Container(
        padding: EdgeInsets.only(top: 100),
          child: Column(
            children: <Widget>[
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                  Column(
                    children: <Widget>[
                      buildBubblesortButton(context, 'BEST CASE', 10000),
                      buildBubblesortButton(context, 'BEST CASE', 30000),
                      buildBubblesortButton(context, 'BEST CASE', 50000),
                    ],
                  ),
                  Column(
                    children: <Widget>[
                      buildBubblesortButton(context, 'WORST CASE', 10000),
                      buildBubblesortButton(context, 'WORST CASE', 30000),
                      buildBubblesortButton(context, 'WORST CASE', 50000),
                    ],
                  )
                ],
              ),
              fibonacciButton(context, 38),
              fibonacciButton(context, 40),
              fibonacciButton(context, 42),
            ],
          ),
        ),
    );
  }
}

Widget buildButton(
    BuildContext context, String text, Color color, String route) {
  return SizedBox(
    width: 150,
    child: RaisedButton(
      elevation: 15,
      color: color,
      textColor: Colors.white,
      child: Text(
        text,
        style: TextStyle(fontSize: 16),
      ),
      onPressed: () => Navigator.pushNamed(context, route),
    ),
  );
}
