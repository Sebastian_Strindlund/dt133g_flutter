import 'package:flutter/material.dart';

@override
Widget fibonacciButton(BuildContext context, int n) {
  return SizedBox(
    width: 150,
    child: RaisedButton(
      elevation: 15,
      color: Colors.indigo,
      textColor: Colors.white,
      child: Text(
        'FIBONACCI($n)',
        // style: TextStyle(fontSize: 16),
      ),
      onPressed: () {
        int start, stop, elapsed;
        start = new DateTime.now().millisecondsSinceEpoch;
        Fibonacci(n);
        stop = new DateTime.now().millisecondsSinceEpoch;
        elapsed = stop - start;
        print("** Flutter Version - Fibonacci($n) time: $elapsed ms **");
      },
    ),
  );
}

int Fibonacci(int val) {
  if (val < 2) {
    return val;
  }
  return Fibonacci(val - 2) + Fibonacci(val - 1);
}
